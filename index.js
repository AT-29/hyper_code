import express from 'express';
import * as socket from 'socket.io';
import bodyParser from 'body-parser';
import { rainbow, createRoom, joinRoom } from './utils/util.js';
const app = express();
const PORT = process.env.PORT || 3030;

app.use(express.static('public'));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/index.html');
});

const users = {};
const rooms = {};

const Server = app.listen(PORT, () => {
  console.log('listening on *:' + PORT);
});


const io = new socket.Server(Server);

//socket events
// io.on('connection', socketEvents);

let count = 0;
io.on('connection', (socket) => {
  console.log('a user connected');
  socket.color = rainbow(Math.random() * 55, Math.random() * 20);
  socket.count = count;
  users[count] = { count: socket.count.toString(), color: socket.color };

  count += 1;
  socket.to(socket.currentRoom).on('disconnect', () => {
    socket
      .to(socket.currentRoom)
      .broadcast.emit('remove user', socket.count + '');
      if(rooms[socket.currentRoom]){
        rooms[socket.currentRoom].count-=1;
        if(rooms[socket.currentRoom].count===0){
          delete rooms[socket.currentRoom];
        }
      }
      delete users[socket.count];
    console.log('user disconnected');
  });
  socket.to(socket.id).on('create room', () => {
    const room = createRoom(socket, rooms);
    io.to(socket.id).emit('send room key', room);
    console.log(socket.id + ' has created room to ' + room);
  });
  socket.to(socket.id).on('join room', (roomId) => {
    const room = joinRoom(socket, roomId, rooms);
    socket.join(room);
    console.log(socket.id + ' has joined room ' + room);
    if(!socket.multiplayer){
      io.to(socket.id).emit('multiplayer',socket.multiplayer);
      socket.multiplayer = true;
    }
  });

  socket.to(socket.currentRoom).on('update room', (content) => {
    socket.to(socket.currentRoom).broadcast.emit('update content', content);
  });


  socket.to(socket.currentRoom).on('send change', (stack) => {
    socket.to(socket.currentRoom).broadcast.emit('get change', {
      color: socket.color,
      count: socket.count + '',
      stack,
    });
  });

  socket.to(socket.currentRoom).on('send user pos', (pos) => {
    socket.to(socket.currentRoom).broadcast.emit('get user pos', {
      color: socket.color,
      count: socket.count + '',
      pos,
    });
  });
});

