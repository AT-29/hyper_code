const rainbow = (numOfSteps, step) => {
  // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
  // Adam Cole, 2011-Sept-14
  // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
  let r, g, b;
  const h = step / numOfSteps;
  const i = ~~(h * 6);
  const f = h * 6 - i;
  const q = 1 - f;
  switch (i % 6) {
    case 0:
      r = 1;
      g = f;
      b = 0;
      break;
    case 1:
      r = q;
      g = 1;
      b = 0;
      break;
    case 2:
      r = 0;
      g = 1;
      b = f;
      break;
    case 3:
      r = 0;
      g = q;
      b = 1;
      break;
    case 4:
      r = f;
      g = 0;
      b = 1;
      break;
    case 5:
      r = 1;
      g = 0;
      b = q;
      break;
  }
  const c =
    '#' +
    ('00' + (~~(r * 255)).toString(16)).slice(-2) +
    ('00' + (~~(g * 255)).toString(16)).slice(-2) +
    ('00' + (~~(b * 255)).toString(16)).slice(-2);
  return c;
};

const revisedRandId = (length = 5) => {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};
const createRoom = (socket, rooms) => {
  socket.currentRoom = revisedRandId(10);
   rooms[socket.currentRoom ] = {roomId:socket.currentRoom, count:0};
   return socket.currentRoom;
};

const joinRoom = (socket, roomId, rooms) => {
  socket.currentRoom = roomId;
  rooms[socket.currentRoom ].count+=1;
  return roomId; 
};
export { rainbow, createRoom, joinRoom };
