import { UI } from './UI/dom_builder.js';
import { randomiseFromPool } from './utils/util.js';
import { dragElement } from './UI/drag_element.js';
const alpha = 0.15;
const prefix = 'rgba(';
const suffix = `,${alpha})`;
const COLORS = ['52, 138, 167', '0, 0,0', '255, 255, 255'];
const colorIndexes = COLORS.map((_, index) => index);
const boxColors = COLORS.map((color) => prefix + color + suffix);
const debugLog = (Code, messure) => {
  const selection = Code.editor.getSelection().trim();
  if (!selection) {
    Code.log('No selection!', '#f3e110');
    return null;
  }

  const output =
    selection[selection.length - 1] === ';'
      ? selection.substr(0, selection.length - 1)
      : selection;

  const data = `
    debug.stack.push(${output});
    if(Array.isArray( debug.stack[ debug.stack.length-1])){
        debug.stack[ debug.stack.length-1] = [... debug.stack[ debug.stack.length-1]];
    }else if(typeof  debug.stack[ debug.stack.length-1] === 'object'){
        debug.stack[ debug.stack.length-1] = {... debug.stack[ debug.stack.length-1]};
    }
    `;
  // Code.editor.replaceSelection("");
  Code.editor.focus();
  const cursor = Code.cursor;

  cursor.ch = Infinity;
  // Code.editor.setValue(this.editor.getValue() + '\n' + data);
  Code.editor.setCursor(cursor);
  Code.editor.replaceSelection(data);
  const source = Code.editor.getValue();
   const tabs = Code.tabs.length>0 ? Code.tabs.join('\n') : '';

  const evaluation =
    '"use strict";  return ((()=>{const debug={stack:[],var:"' +
    output +
    '"}; ' +
    tabs +
    source +
    '; return debug})())';
  let result;
  let messuring;
  try {
    if (messure) {
      const t0 = performance.now();
      result = new Function(evaluation)();
      const t1 = performance.now();
      messuring = (t1 - t0).toFixed(3);
      console.log('Took: ' + messuring + ' milliseconds.');
    } else {
      result = new Function(evaluation)();
    }
  } catch (err) {
    console.log('debugging failed because of ' + err);
  } finally {
    Code.editor.undo();
  }

  if (!result) {
    return Code.log(
      'Invalid selection, failed code run or interupting return',
      '#ff275d'
    );
  } else if (result && result.stack.length === 0) {
    return Code.log(result.var + ' never gets logged!', '#f3e110');
  }

  const rdmIndex = randomiseFromPool(colorIndexes);
  const div = UI.create('div', {
    class: 'log_box',
    style: 'background-color:' + boxColors[rdmIndex],
  });
  const dragg = UI.create('button', div, {
    class: 'ui',
    style:
      'padding:5px;cursor:pointer;font-size:30px;width:50px;height:50px;text-align:center;',
    textContent: '⁜',
    drag: 'false',
  });
  UI.create('p', div);

  UI.create('input', div, {
    class: 'ui',
    value: result.var,
    style: 'text-align:center',
    disabled: 'disabled',
  });

  dragg.addEventListener('click', () => {
    const drag = dragg.getAttribute('drag');
    if (drag === 'false') {
      dragg.setAttribute('drag', 'true');
      dragg.textContent = '❖';
    } else {
      dragg.setAttribute('drag', 'false');
      dragg.textContent = '⁜';
    }
  });
  UI.create('p', div);
  UI.create('textarea', div, {
    cols: 20,
    rows: 5,
    class: 'log_line',
    innerHTML: result.stack.reduce(
      (acc, item) => (acc += JSON.stringify(item) + '\n'),
      ''
    ),
  });
  UI.create('p', div, {
    class: 'ui',
    textContent: 'Took: ' + messuring + ' mills',
  });

  dragElement(div, dragg);
  Code.editor.addWidget(cursor, div, true);
  Code.log('Log successful', `rgb(${COLORS[rdmIndex]})`);
  setTimeout(() => Code.log(''), 1000);
  Code.changesStack.pop();
  Code.changesStack.pop();

  return result;
};

const clearLogs = (className='log_box') => {
  console.clear();
  // [...document.getElementsByClassName('log_line')].forEach(item=>item.parentNode.removeChild(item));
  [...document.getElementsByClassName(className)].forEach((item) =>
    item.parentNode.removeChild(item)
  );
};



export { debugLog, clearLogs };
