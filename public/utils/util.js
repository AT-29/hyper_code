const randomiseFromPool = (pool) => {
  return pool[Math.floor(Math.random() * pool.length)];
};
export { randomiseFromPool };
