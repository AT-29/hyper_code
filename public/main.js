import { UI } from './UI/dom_builder.js';
import { clearLogs, debugLog } from './debugger.js';
import { Interface } from './editor.js';

const socket = io();

window.addEventListener('load', () => {
  const users = new Map();
  UI.setParent(document.getElementById('app_container'));
  UI.create('button', {
    textContent: '▷',
    class: 'ui',
    style: 'width:55px;heigth:25px; margin-right:1%',
  }).addEventListener('click', () => Code.runCode());
  UI.create('button', { textContent: 'log', class: 'ui' }).addEventListener(
    'click',
    () => {
      const debug = debugLog(Code, true);
      if (!debug) return;
      debug.stack.forEach((value, index) => {
        console.log(index + '. ' + debug.var + ' :', value);
      });
    }
  );
  UI.create('button', {
    textContent: 'clear log',
    class: 'ui',
  }).addEventListener('click', () => {
    clearLogs('log_box');
  });

  UI.create('input', {
    id: 'console',
    disabled: true,
    class: 'ui',
    style: 'width:80%',
  });

  const colorThemes = document.getElementById('colthemes');

  const Code = new Interface(
    document.getElementById('app_container'),
    document.getElementById('console'),
    {
      value: '',
      mode: 'javascript',
      tabSize: 5,
      theme: 'dracula',
      lineNumbers: true,
      styleActiveLine: true,
      matchBrackets: true,
      onGutterClick: CodeMirror.newFoldFunction(CodeMirror.braceRangeFinder),
      extraKeys: {},
      gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
      lint: false,
      foldGutter: true,
    }
  );

  Code.editor.setSize(1750, 800);

  const themeChoice =
    (location.hash && location.hash.slice(1)) ||
    (document.location.search &&
      decodeURIComponent(document.location.search.slice(1)));
  if (themeChoice) {
    colorThemes.value = themeChoice;
    Code.editor.setOption('theme', themeChoice);
  }
  CodeMirror.on(window, 'hashchange', function () {
    const theme = location.hash.slice(1);
    if (theme) {
      colorThemes.value = theme;
      Code.changeTheme(
        colorThemes.options[colorThemes.selectedIndex].textContent
      );
    }
  });

  const inputField = UI.create('input', {
    id: 'input_field',
    class: 'ui',
    style: 'width:10%;margin-top:1%',
  });
  UI.create('button', { class: 'ui', textContent: 'get' }).addEventListener(
    'click',
    () => {
      Code.loadSnippet(inputField.value);
    }
  );

  colorThemes.addEventListener('change', () =>
    Code.changeTheme(colorThemes.options[colorThemes.selectedIndex].textContent)
  );

  inputField.addEventListener('keypress', (e) => {
    if (e.key === 'Enter' && document.activeElement === inputField) {
      Code.loadSnippet(inputField.value);
    }
  });

  document.addEventListener('keydown', (e) => {
    if (e.key.toLowerCase() === 'q' && e.ctrlKey) Code.runCode();
  });

  UI.create('button', {
    textContent: 'json',
    class: 'ui',
  }).addEventListener('click', () => Code.json());
  UI.create('button', {
    textContent: '++tabs',
    class: 'ui',
  }).addEventListener('click', () => Code.openTabs());
  UI.create('button', {
    textContent: '--tabs',
    class: 'ui',
  }).addEventListener('click', () =>{
    clearLogs('tab_box');
    Code.tabs.length = 0;
  } );
  UI.create('button', {
    textContent: 'mdn',
    class: 'ui',
    style: 'margin-left:2%',
  }).addEventListener('click', () =>
    Code.OpenExternalResource('mdn', inputField.value)
  );
  UI.create('button', {
    textContent: 'stackover',
    class: 'ui',
  }).addEventListener('click', () =>
    Code.OpenExternalResource('stackoverflow', inputField.value)
  );
  UI.create('button', {
    textContent: 'doc',
    class: 'ui',
  }).addEventListener('click', () => Code.readDoc());

  //thisi s just because i'm lazy to make it dynamic;
  document.getElementById('colthemes').style.display = 'inline-block';

  //Just for testing
  // const SendCodeTest = UI.create('button',{textContent:'send-test', class:'ui', style:'margin-left:2%' });




  const multiplayer = () =>{

    UI.create('button', document.getElementById('settings'), {
      class: 'ui',
      textContent: 'update',
      style:'margin-left:2%',
    }).addEventListener('click', () => {
      socket.emit('update room', Code.editor.getValue());
     
    });
  
    socket.on('remove user', (count) => {
      const user = users.get(count);
      if (user) {
        user.marker.clear();
        users.delete(count);
      }
    });
  
    Code.editor.on('change', function (cm, change) {
      const coords = { from: change.from, to: change.to };
      const text = change.text;
      Code.changesStack.push({ text, coords });
    });
  
    Code.editor.on('keyup', function (cm, event) {
      if (!Code.trot && Code.changesStack.length !== 0) {
        Code.trot = setTimeout(() => {
          socket.emit('send change', Code.changesStack);
          Code.trot = null;
          Code.changesStack.length = 0;
        }, Interface.TROT_TIME * 1000);
      }else{
        socket.emit('send user pos', Code.editor.getCursor());
      }
    });
    socket.on('update content', (content) => {
      Code.editor.setValue(content);  
      Code.changesStack.pop();
    });
    socket.on('get change', (data) => {
      data.stack.map((item) => {
        Code.editor.doc.replaceRange(item.text, item.coords.from, item.coords.to);
        Code.changesStack.pop();
      });
    });
  
    
    socket.on('get user pos', (data) => {
      const user = users.get(data.count);
      if (user && user.marker) {
        user.marker.clear();
        user.marker = Code.editor.setBookmark(
          { line: data.pos.line, ch: 0 },
          { widget: user.span }
        );
      }else{
        users.set(data.count, { span: null, marker: null });
        const user = users.get(data.count);
        const cursorCoords = Code.editor.cursorCoords({ line: 0, ch: 0 });
        user.span = UI.create('span', {
          style: `border-left:2px solid ${data.color};${
            cursorCoords.bottom - cursorCoords.top
          }px;padding:0;z-index:0`,
        });
        user.marker = Code.editor.setBookmark(
          { line: 0, ch: 0 },
          { widget: user.span }
        );
      }
    });
  
  }




  const roomInput = UI.create('input', document.getElementById('settings'), {
    class: 'ui',
  });
  UI.create('button', document.getElementById('settings'), {
    class: 'ui',
    textContent: 'join room',
  }).addEventListener('click', () => {
    Code.editor.setValue('');
    socket.emit('join room', roomInput.value);
    for (const user of users) {
      if (user) {
        user[1].marker.clear();
        users.delete(user[0]);
      }
    }
  
  });
  socket.on('multiplayer',(isConnected)=>{
    if(!isConnected) multiplayer();
  });
  
  UI.create('button', document.getElementById('settings'), {
    class: 'ui',
    textContent: 'create room',
  }).addEventListener('click', () => {
    socket.emit('create room');
  });
  socket.on('send room key', (room) => {
    roomInput.value = room;
  });







  
});
