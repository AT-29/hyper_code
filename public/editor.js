import { UI } from './UI/dom_builder.js';
import { dragElement } from './UI/drag_element.js';

export class Interface {
  #editor;
  #output;
  #documentation = new Map();
  #LineOffset = 2;
  changesStack = [];
  tabs = [];
  static TROT_TIME = 1;
  #trot = null;

  constructor(divEl, outputEl, options) {
    this.#editor = CodeMirror(divEl, options);
    this.#output = outputEl;
  }

  get editor() {
    return this.#editor;
  }

  get cursor() {
    return { ...this.editor.getCursor() };
  }

  get documentation() {
    return this.#documentation;
  }

  set trot(timeout) {
    this.#trot = timeout;
  }
  get trot() {
    return this.#trot;
  }

  log(value, color) {
    this.#output.value = value;
    this.#output.style.color = color;
  }

  runCode(source = this.editor.getValue()) {
    try {
      const tabs = this.tabs.length>0 ? this.tabs.join('\n') : '';
      const result = new Function(
        '"use strict"; return ((()=>{ ' + 
          tabs +
          source +
          ';return ' +
          this.editor.getSelection() +
          '})())'
      )();
      this.log(JSON.stringify(result), 'rgb(85, 226, 100)');
    } catch (err) {
      const stack = err.stack.split('<anonymous>:');
      if (err.stack && stack[1]) {
        const offset = this.#LineOffset;
        const stackLen = stack.length;
        const errCoords = err.stack
          .split('mous>:')
          [stackLen - 2].split(')')[0]
          .split(':');
        const line = errCoords[0] - offset;
        const ch = errCoords[1];
        const ERROR_MSG =
          err.stack.split('Error:')[0] +
          'Error at line ' +
          line +
          ': ' +
          err.message;
        const errorEl = UI.create('button', {
          id: 'error_line',
          innerHTML: ' ' + err.message,
        });
        this.editor.addWidget({ line, ch: Infinity }, errorEl, true);
        setTimeout(() => {
          errorEl.parentNode.removeChild(errorEl);
        }, 3000);

        this.editor.focus();
        this.log(ERROR_MSG, '#ff275d');
      } else {
        this.log(
          err.stack.split('Error:')[0] + 'Error: ' + err.message,
          '#ff275d'
        );
        console.error(err);
      }
    }
  }

  changeTheme(theme) {
    this.editor.setOption('theme', theme);
    location.hash = '#' + theme;
  }
  getSnippetsFromExternal(external) {
    if (external) {
      const link = `https://raw.githubusercontent.com/AnthonyTonev/hyper_code_snippets/master/${external}.json`;
      fetch(link)
        .then((response) => response.json())
        .then((data) => {
          this.editor.setValue(
            this.editor.getValue() + '\n' + data.code + '\n'
          );
          this.editor.scrollIntoView({ line: this.editor.lastLine(), char: 0 });
          this.log(data.title + ' imported!', '#ffffff');
          if (data.description) {
            data.description.map((item) => {
              this.#documentation.set(item.id, {
                description: item.description,
                example: item.example,
              });
            });
          }
        })
        .catch((err) => {
          this.log(
            'Snippet does not exist or it has bad JSON format.',
            '#f3e110'
          );
        });
      return;
    }
  }
  getSnippet(input) {}
  loadSnippet(input) {
    this.getSnippetsFromExternal(input.trim());
  }
  json() {
    const data = `{
        "title": "untitled",
        "description": "",
        "code":${JSON.stringify(this.editor.getValue())}
}`;
    this.editor.setValue(data);
  }
  OpenExternalResource(resource, search) {
    let link;
    switch (resource) {
      case 'mdn':
        link = 'https://developer.mozilla.org/en-US/';
        break;
      case 'stackoverflow':
        link = 'https://stackoverflow.com/';
        break;
    }

    window.open(
      link + `search?q=${search}`,
      'External Resource',
      'toolbar=no,width=600,height=800,left=1300,top=150,status=no,scrollbars=no,resize=no'
    );
  }

  
  modifySourceCodeBeforeParsing(source) {
    let modA = source.replace(/import /gi, '//import ');
    return modA.replace(/export /gi, '//export ');
  }

  readDoc() {
    const selection = this.editor.getSelection();
    const documentation = this.documentation.get(selection);
    this.log('Got documentation for ' + selection, '#ffffff');
    if (!documentation)
      return this.log(
        'Documentation for ' + selection + ' does not exist.',
        '#f3e110'
      );

    //TODO make this reusable also in debugger
    const div = UI.create('div', {
      class: 'log_box',
      style: 'background-color:rgba(82, 255, 238,0.3)',
    });
    const dragg = UI.create('button', div, {
      class: 'ui',
      style:
        'padding:5px;cursor:pointer;font-size:30px;width:50px;height:50px;text-align:center;',
      textContent: '⁜',
      drag: 'false',
    });
    UI.create('p', div);

    UI.create('input', div, {
      class: 'ui',
      value: selection,
      style: 'text-align:center',
      disabled: 'disabled',
    });

    dragg.addEventListener('click', () => {
      const drag = dragg.getAttribute('drag');
      if (drag === 'false') {
        dragg.setAttribute('drag', 'true');
        dragg.textContent = '❖';
      } else {
        dragg.setAttribute('drag', 'false');
        dragg.textContent = '⁜';
      }
    });
    UI.create('p', div);
    UI.create('textarea', div, {
      cols: 65,
      rows: 3,
      class: 'ui',
      innerHTML: documentation.description,
    });
    UI.create('p', div);
    UI.create('textarea', div, {
      cols: 65,
      rows: 11,
      class: 'log_line',
      innerHTML: documentation.example,
    });

    dragElement(div, dragg);
    this.editor.addWidget(this.cursor, div, true);
  }



  openTabs() {
    const selection = this.editor.getSelection();
    this.editor.replaceSelection('');
    this.tabs.push(selection);
    //TODO make this reusable also in debugger
    const div = UI.create('div', {
      class: 'tab_box',
      style: 'background-color:rgba(82, 255, 238,0.15)',
    });
    const dragg = UI.create('button', div, {
      class: 'ui',
      style:
        'cursor:pointer;font-size:30px;width:50px;height:50px;text-align:center;',
      textContent: '⁜',
      drag: 'false',
    });
    UI.create('p', div);
    UI.create('button', div, {
      class: 'ui',
      value: this.tabs.length,
      style: 'text-align:center;cursor:pointer',
    
      innerHTML:'[ - ] '
    }).addEventListener('click',(e)=>{
      console.log(this.tabs)
      this.editor.setValue(this.editor.getValue()+'\n'+this.tabs[e.target.value-1].trim());
      this.tabs[e.target.value-1] = "";
      document.getElementById('tab_'+e.target.value).value = '';
    });
    UI.create('input', div, {
      class: 'ui',
      value: this.tabs.length,
      style: 'text-align:center;',
     
    })
    UI.create('button', div, {
      class: 'ui',
      value: this.tabs.length,
      style: 'text-align:center;cursor:pointer;',
      innerHTML:'[ + ]'
    }).addEventListener('click',(e)=>{
      const selection = this.editor.getSelection().trim();
      this.editor.replaceSelection('');
      this.tabs[e.target.value-1] = selection;
      document.getElementById('tab_'+e.target.value).value = selection;
    });
    dragg.addEventListener('click', () => {
      const drag = dragg.getAttribute('drag');
      if (drag === 'false') {
        dragg.setAttribute('drag', 'true');
        dragg.textContent = '❖';
      } else {
        dragg.setAttribute('drag', 'false');
        dragg.textContent = '⁜';
      }
    });
    UI.create('p', div);

    UI.create('textarea', div, {
      cols: 45,
      rows: 25,
      id:'tab_'+this.tabs.length,
      class: 'tab_line',
      innerHTML: this.tabs[this.tabs.length-1],
    });

    dragElement(div, dragg);
    this.editor.addWidget(this.cursor, div, true);
  }
}
